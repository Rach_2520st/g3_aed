MEZCLA DE LISTAS ENLAZADAS:

este programa se encarga de ingresar dos listas enlazada y luego mezclarlas para mostrarlas.

para acceder al programa se debe clonar este repositorio, luego de esto ingresar al directorio "E_2", despues de esto ejecutar el comando make y luego acceder al programa con "./Programa"


Requisitos:
Sistema operativo Linux
Herramienta de gestion de dependencias make (para Makefile)
Compilador GNU C++ (g++)

Construido y probado con:
Ubuntu 18.04.03 LTS
gcc y g++ version 7.4.0
GNU Make 4.1
Editor utilizado: Sublime Text

Autor: Rachell Aravena.
