LISTA ENLAZADA ORDENADA:
este programa consiste en crear un lista enlazada ordenada, consta de una clase llamada Lista que es la encargada de crear, ordenar e imprimir la lista a medida que se van ingresando los numeros enteros.
Para su ejecución se debe clonar el presente repositorio, luego de esto acceder al repositorio "E_1" y luego ejecutar el comando make, despues de esto acceder al programa con "./Programa", y luego ejecutar las acciones que va pidiendo el correspondiente programa.

Requisitos:
Sistema operativo Linux
Herramienta de gestion de dependencias make (para Makefile)
Compilador GNU C++ (g++)

Construido y probado con:
Ubuntu 18.04.03 LTS
gcc y g++ version 7.4.0
GNU Make 4.1
Editor utilizado: Sublime Text

Autor: Rachell Aravena.
